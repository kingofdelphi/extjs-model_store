
Ext.define('MyApp.view.main.MainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.main',

    click: function (ticker) {
        console.log(`${ticker} clicked`);
        if (ticker === 'ADD') {
            const store = this.getStore('tickers');
            store.add('NEW');
        } else {

        }
    },
    addBoard: function () {
        const view = this.getView();
        const comp = Ext.create('MyApp.view.main.TicTacToe');
        view.add(comp);
    },
});