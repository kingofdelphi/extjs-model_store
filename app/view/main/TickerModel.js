Ext.define('MyApp.model.TickerModel', {
    extend: 'Ext.data.Model',
    fields: ['ticker']
});