Ext.define('MyApp.view.main.Main', {
    extend: 'Ext.panel.Panel',

    // Always use this form when defining a view class. This
    // allows the creator of the component to pass data without
    // erasing the ViewModel type that we want.
    viewModel: {
        type: 'test',
        data: {
            tickers: ['ADD', 'BOKL', 'ADBL', 'SCB', 'NABIL', 'CHCL'],
        },
    },

    controller: 'main',
    listeners: {
        click: 'click',
    },
    bind: {
        title: 'Tickers',
        items: '{getTickers}'
    },

    defaultType: 'textfield',
    items: [
        {
            xtype: 'button',
            text: 'Add Ticker',
        }
    ]
});