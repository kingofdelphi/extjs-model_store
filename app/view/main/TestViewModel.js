Ext.define('MyApp.view.TestViewModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.test', // connects to viewModel/type below

    stores: {
        tickers: {
            proxy: {
                type: 'ajax',
                url: '/users.json',
                reader: {
                    type: 'json',
                    rootProperty: 'tickerList',
                }
            },
            autoLoad: true
        },
    },

    formulas: {
        // We'll explain formulas in more detail soon.
        getTickers: {
            bind: {
                bindTo: '{tickers}',
                deep: true
            },
            get: function (store) {
                const data = store.getData();
                const view = this.getView();
                // console.log(data);
                const res = data.items.map(d => d.data).map(ticker => ({
                    xtype: 'button',
                    text: ticker,
                    handler: function () {
                        view.fireEvent('click', ticker);
                    },
                }));
                return res;
            }
        },
    }
});